AdSpecsRails::Application.routes.draw do
  resources :modals
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  resources :ads
  root to: "ads#index"
  match "/new",     to: "ads#new",                    via: "get"
  # match '/signup',  to: 'users#new',                  via: 'get'
  match '/signin',  to: 'sessions#new',               via: 'get'
  match '/signout', to: 'sessions#destroy',           via: 'delete'
  match "*a",       to: "application#routing_error",  via: 'get'
end
