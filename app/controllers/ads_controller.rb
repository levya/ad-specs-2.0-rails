class AdsController < ApplicationController
  include SessionsHelper
  before_action :signed_in_user
  skip_before_action :signed_in_user, only: [:index, :show]
  def show
    @ad = Ad.find(params[:id])
  end
  def index
    @ads = Ad.all
    respond_to do |format|
      format.html
      format.csv { send_data @ads.to_csv }
      format.xls
      format.json do
        render :json => @ads
      end
      format.pdf do
        render :pdf => "TWPN_Ads"
      end
    end
  end
  def new
    @ad = Ad.new
  end
  def create
    @ad = Ad.create params[:ad]
    if @ad.save
      redirect_to @ad, notice: "Ad successfully created!"
    else
      render 'new' 
    end
  end 
  def edit
    @ad = Ad.find(params[:id])
  end
  def update
    @ad = Ad.find(params[:id])
    if @ad.update_attributes(params[:ad])
      redirect_to @ad, notice: "Ad successfully saved!"
    else
      render 'edit'
    end
  end
  def destroy
    Ad.find(params[:id]).destroy
    redirect_to root_url
  end
end 