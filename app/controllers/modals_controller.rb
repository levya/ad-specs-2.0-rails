class ModalsController < ApplicationController
	include SessionsHelper
	before_action :signed_in_user
	# GET /modals
	# GET /modals.json
  
	def index
		@modals = Modal.all
	end

	# GET /modals/1
	# GET /modals/1.json
	def show
		@modal = Modal.find(params[:id])
	end

	# GET /modals/new
	def new
		@modal = Modal.new
	end

	# GET /modals/1/edit
	def edit
		@modal = Modal.find(params[:id])
	end

	# POST /modals
	# POST /modals.json
	def create
		@modal = Modal.new(modal_params)
		if @modal.save
			redirect_to @modal, notice: 'New category created! Hot stuff!'
		else
			render action: 'new'
		end
	end

	# PATCH/PUT /modals/1
	# PATCH/PUT /modals/1.json
	def update
		@modal = Modal.find(params[:id])
		if @modal.update_attributes(params[:modal])
			redirect_to root_url, notice: 'Saved! High five!'
		else
			render 'edit'
		end
	end

	# DELETE /modals/1
	# DELETE /modals/1.json
	def destroy
		Modal.find(params[:id]).destroy
		redirect_to root_url
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_modal
		@modal = Modal.find(params[:id])
	end

	# Never trust parameters from the scary internet, only allow the white list through.
	def modal_params
		params.require(:modal).permit(:name, :title, :category, :contents)
	end
end
