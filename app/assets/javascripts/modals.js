//WYSIWYG Text Editor
$(document).ready(function() {
  
  if($('textarea#modal_contents').length) {
    var myCustomTemplates = {
      html : function(locale) {
        return "<li>" +
               "<div class='btn-group'>" +
               "<a class='btn' data-wysihtml5-action='change_view' title='" + locale.html.edit + "'><span class='glyphicon glyphicon-pencil'></span></a>" +
               "</div>" +
               "</li>";
      },
      lists : function(locale) {
        return "<li>" +
               "<div class='btn-group'>" +
               "<a class='btn' data-wysihtml5-command='insertUnorderedList' title='" + locale.lists.edit + "'><span class='glyphicon glyphicon-list'></span></a>" +
               "<a class='btn' data-wysihtml5-command='insertOrderedList' title='" + locale.lists.edit + "'><span class='glyphicon glyphicon-list-alt'></span></a>" +
               "<a class='btn' data-wysihtml5-command='Outdent' title='" + locale.lists.edit + "'><span class='glyphicon glyphicon-indent-right'></span></a>" +
               "<a class='btn' data-wysihtml5-command='Indent' title='" + locale.lists.edit + "'><span class='glyphicon glyphicon-indent-left'></span></a>" +
               "</div>" +
               "</li>";
      },
      link : function(locale, options) {
        var size = (options && options.size) ? ' btn-'+options.size : '';
        return "<li>" +
                  "<div class='bootstrap-wysihtml5-insert-link-modal modal fade'>" +
                    "<div class='modal-dialog'>" + 
                      "<div class='modal-content'>" +
                        "<div class='modal-header'>" +
                          "<a class='close' data-dismiss='modal'>&times;</a>" +
                          "<h3>" + locale.link.insert + "</h3>" +
                        "</div>" +
                        "<div class='modal-body'>" +
                          "<input value='http://' class='bootstrap-wysihtml5-insert-link-url input-xlarge'>" +
                          "<label class='checkbox'> <input type='checkbox' class='bootstrap-wysihtml5-insert-link-target' checked>" + locale.link.target + "</label>" +
                        "</div>" +
                        "<div class='modal-footer'>" +
                          "<a href='#' class='btn' data-dismiss='modal'>" + locale.link.cancel + "</a>" +
                          "<a href='#' class='btn btn-primary' data-dismiss='modal'>" + locale.link.insert + "</a>" +
                        "</div>" +
                      "</div>" +
                    "</div>" +
                  "</div>" +
                  "<a class='btn" + size + "' data-wysihtml5-command='createLink' title='" + locale.link.insert + "' tabindex='-1'><span class='glyphicon glyphicon-cloud-upload'></span></a>" +
                "</li>";
        },
    }
    $('textarea#modal_contents').wysihtml5({
      "html": true,
      "image": false,
      customTemplates: myCustomTemplates
    });
  }
  
});