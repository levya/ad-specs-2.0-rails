class Modal < ActiveRecord::Base
	attr_protected
	validates :name, presence: true
	validates :category, presence: true
end
