module ApplicationHelper
	# Returns...the title.
	def full_title(page_title)
		base_title = "| The Washington Post Company"
		if page_title.empty?
			"Ad Specs #{base_title}"
		else
			"#{page_title} #{base_title}"
		end
	end
	def link_maker(category)
		case category
		when "Standard Ads"
			link_to "Standard Ads", "/#modal-standard", :data => {:toggle => "modal"}
		when "Email"
			link_to "Email", "/#modal-email", :data => {:toggle => "modal"}
		when "Mobile"
			link_to "Mobile", "/#modal-mobile", :data => {:toggle => "modal"}
		when "Video"
			link_to "Video", "/#modal-video", :data => {:toggle => "modal"}
		when "Feature Bars, Tiles & Misc."
			link_to "Feature Bars, Tiles & Misc.", "/#modal-features", :data => {:toggle => "modal"}
		when "Intrusive Ads"
			link_to "Intrusive Ads", "/#modal-intrusive", :data => {:toggle => "modal"}
		else
			category
		end
	end
  def bootstrap_class_for flash_type
    case flash_type
    when :success
      "alert-success"
    when :error
      "alert-danger"
    when :alert
      "alert-warning"
    when :notice
      "alert-success"
    else
      flash_type.to_s
    end
  end
end
