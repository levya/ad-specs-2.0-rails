class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string :name
      t.string :category
      t.string :size
      t.string :expanded_size
      t.string :file_size
      t.boolean :animation
      t.boolean :third_party
      t.text :notes
      t.text :deadline

      t.timestamps
    end
  end
end
