class ChangeFieldsFromBooleanToString < ActiveRecord::Migration
  def change
		change_column :ads, :animation, :string
		change_column :ads, :third_party, :string
  end
end
