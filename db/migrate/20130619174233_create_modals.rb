class CreateModals < ActiveRecord::Migration
  def change
    create_table :modals do |t|
      t.string :name
      t.string :title
      t.text :contents

      t.timestamps
    end
  end
end
