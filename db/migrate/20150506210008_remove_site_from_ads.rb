class RemoveSiteFromAds < ActiveRecord::Migration
  def change
    remove_column :ads, :on_post
    remove_column :ads, :on_slate
    remove_column :ads, :on_root
  end
end