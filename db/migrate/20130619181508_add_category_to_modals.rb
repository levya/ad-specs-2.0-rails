class AddCategoryToModals < ActiveRecord::Migration
  def change
    add_column :modals, :category, :string
  end
end
