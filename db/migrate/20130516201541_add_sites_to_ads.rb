class AddSitesToAds < ActiveRecord::Migration
  def change
    add_column :ads, :on_post, :boolean
    add_column :ads, :on_slate, :boolean
    add_column :ads, :on_root, :boolean
  end
end
